<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
    <channel>
        <title>TUD / ZIH</title>
        <link>https://bs.zih.tu-dresden.de/</link>
        <description>Neue Ankündigungen / Meldungen des ZIH zum Thema Betriebsstatus sowie zu Störungen</description>
        <language>de</language>
        <copyright>TU Dresden - ZIH</copyright>
        <lastBuildDate>Tue, 09 Nov 2021 10:08:19 +0100</lastBuildDate>

        <item>
			<title><![CDATA[Hardware-Erneuerung VoIP-Server (anstehend)]]></title>
			<description><![CDATA[vom 22.11.2021 / 00.00 Uhr bis 23.11.2021 / 23.59 Uhr<br><br>Vom 22.11.2021 bis 23.11.2021 jeweils 00:00 - 24:00 Uhr werden Wartungsarbeiten an den VoIP-Servern durchgeführt. <br />
<br />
Die Arbeiten erfolgen unterbrechungsfrei. Telefone werden sich jeweils auf den Redundanzservern ...]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1122</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1122</guid>
			<pubDate>Thu, 04 Nov 2021 14:10:29 +0100</pubDate>
		</item>
		<item>
			<title><![CDATA[Systemwartung GoToMeeting (anstehend)]]></title>
			<description><![CDATA[vom 14.11.2021 / 06.00 Uhr bis 14.11.2021 / 12.00 Uhr<br><br>Zweck der Wartung: Erhalt der Systemleistung und -stabilität.<br />
Dauer: Die Arbeiten werden in einem Zeitfenster von 6 Stunden erledigt. <br />
Auswirkungen: Während dieser Zeit können Benutzer zwei Stunden lang keine ...]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1114</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1114</guid>
			<pubDate>Mon, 18 Oct 2021 16:09:51 +0200</pubDate>
		</item>
		<item>
			<title><![CDATA[Systemwartung GoToMeeting (anstehend)]]></title>
			<description><![CDATA[vom 13.11.2021 / 06.00 Uhr bis 13.11.2021 / 12.00 Uhr<br><br>Zweck der Wartung: Erhalt der Systemleistung und -stabilität.<br />
Dauer: Die Arbeiten werden in einem Zeitfenster von 6 Stunden erledigt. <br />
Auswirkungen: Während dieser Zeit können Benutzer zwei Stunden lang keine ...]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1113</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1113</guid>
			<pubDate>Mon, 18 Oct 2021 16:08:35 +0200</pubDate>
		</item>
		<item>
			<title><![CDATA[Ticketsystem: Wartungsarbeiten am 10.11.2021 (anstehend)]]></title>
			<description><![CDATA[vom 10.11.2021 / 08.00 Uhr bis 10.11.2021 / 09.00 Uhr<br><br>Das Ticketsystem steht aufgrund von Wartungsarbeiten am Mittwoch, den 10.11.2021<br />
in der Zeit von 8:00 bis 9:00 Uhr nicht zur Verfügung.]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1119</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1119</guid>
			<pubDate>Mon, 25 Oct 2021 18:38:15 +0200</pubDate>
		</item>
		<item>
			<title><![CDATA[Umbau ELT-UV im ZEU (beendet)]]></title>
			<description><![CDATA[vom 09.11.2021 / 09.00 Uhr bis 09.11.2021 / 10.00 Uhr<br><br>Am 09.11.21 wird die ELT-UV wegen Umbauarbeiten im ZEU Raum 11 abgeschaltet. Die Abschaltung wird gegen 9Uhr sein und ist für ca. 1h geplant.<br />
<br />
Die Abschaltung wird so vorbereitet, dass ein Ausfall des Routers sowie des ...]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1121</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1121</guid>
			<pubDate>Thu, 04 Nov 2021 14:06:37 +0100</pubDate>
		</item>
		<item>
			<title><![CDATA[Allgemeine Störung bei der Nutzung des Selfserviceportals]]></title>
			<description><![CDATA[vom 09.11.2021 / 08.46 Uhr bis 10.11.2021 / 12.00 Uhr<br><br>Bei der Nutzung des Selfserviceportal kommt es zur Zeit zu Störungen und Fehlermeldungen.<br />
<br />
Zur Erstellung / Buchung von BBB-Videokonferenzräumen können Sie auf das BBB-Portal zur Buchung fester Räume bis 50 ...]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1126</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1126</guid>
			<pubDate>Tue, 09 Nov 2021 08:49:48 +0100</pubDate>
		</item>
		<item>
			<title><![CDATA[Zertifikatsbeantragung über Selfserviceportal vorübergehend nicht verfügbar]]></title>
			<description><![CDATA[vom 05.11.2021 / 11.35 Uhr bis 10.11.2021 / 16.00 Uhr<br><br>Der Dienst zur Beantragung von Zertifikaten über EVAZert im Selfserviceportal ist vorübergehend nicht verfügbar.<br />
Die Kollegen arbeiten an der Lösung des Problems.]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1125</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1125</guid>
			<pubDate>Mon, 08 Nov 2021 11:53:00 +0100</pubDate>
		</item>
		<item>
			<title><![CDATA[Ticketsystem: Wartungsarbeiten am 03.11.2021 (beendet)]]></title>
			<description><![CDATA[vom 03.11.2021 / 08.00 Uhr bis 03.11.2021 / 09.00 Uhr<br><br>Das Ticketsystem steht aufgrund von Wartungsarbeiten am Mittwoch, den 03.11.2021<br />
in der Zeit von 8:00 bis 9:00 Uhr nicht zur Verfügung.]]></description>
			<link>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1118</link>
			<author>servicedesk@tu-dresden.de (Servicedesk der TU Dresden)</author>
			<guid>https://bs.zih.tu-dresden.de/?action=newsdet&amp;news_id=1118</guid>
			<pubDate>Mon, 25 Oct 2021 18:33:34 +0200</pubDate>
		</item>
    </channel>
</rss>
