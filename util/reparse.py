import json
import pprint
import collections
import progressbar

# python -m spacy download de_dep_news_trf

pp = pprint.PrettyPrinter(indent=4)


def is_quote(content):
    if content[:2] == "> ":
        return True
    else:
        return False


def remove_markdown(content):
    content = content.replace("*", "")
    content = content.replace("_", "")
    content = content.replace("#", "")
    return content


def is_text(message):
    if not message["type"] == "m.room.message":
        return False
    if not message["content"]:
        return False
    if not message["content"]["msgtype"] == "m.text":
        return False
    return True


def print_first_n(data, n):
    for elem in sorted(data.items(), key=lambda x: x[1])[:n]:
        print(elem)


nouns = collections.defaultdict(int)
adverbs = collections.defaultdict(int)
verbs = collections.defaultdict(int)
adjektivs = collections.defaultdict(int)

with open("matrix-export-9.11.2021 um 18-53-28.json") as f:
    chat_data = json.load(f)

for message in chat_data["messages"]:
    try:
        if is_text(message):
            for line in message["content"]["body"].split("\n"):
                if is_quote(line):
                    continue
                line = remove_markdown(line)
                if "funktionier" in line.lower() and "nicht" in line.lower():
                    pp.pprint(line)

    except KeyError:
        pp.pprint(message)
        raise
