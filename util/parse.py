import json
import pprint
import collections
import progressbar

import spacy
nlp = spacy.load("de_dep_news_trf")

# python -m spacy download de_dep_news_trf

pp = pprint.PrettyPrinter(indent=4)


def is_quote(content):
    if content[:2] == "> ":
        return True
    else:
        return False


def remove_markdown(content):
    content = content.replace("*", "")
    content = content.replace("_", "")
    content = content.replace("#", "")
    return content


def is_text(message):
    if not message["type"] == "m.room.message":
        return False
    if not message["content"]:
        return False
    if not message["content"]["msgtype"] == "m.text":
        return False
    return True


def print_first_n(data, n):
    for elem in sorted(data.items(), key=lambda x: x[1], reverse=True)[:n]:
        print(elem)


nouns = collections.defaultdict(int)
adverbs = collections.defaultdict(int)
verbs = collections.defaultdict(int)
adjektivs = collections.defaultdict(int)

with open("matrix-export-9.11.2021 um 18-53-28.json") as f:
    chat_data = json.load(f)

for message in progressbar.progressbar(chat_data["messages"]):
    try:
        if is_text(message):
            for line in message["content"]["body"].split("\n"):
                if is_quote(line):
                    continue
                line = remove_markdown(line)
                doc = nlp(line)
                for token in doc:
                    if token.pos_ == "NOUN":
                        nouns[token.lemma_.lower()] += 1
                    elif token.pos_ == "VERB":
                        verbs[token.lemma_.lower()] += 1
                    elif token.pos_ == "ADV":
                        adverbs[token.lemma_.lower()] += 1
                    elif token.pos_ == "ADJ":
                        adjektivs[token.lemma_.lower()] += 1
    except KeyError:
        pp.pprint(message)
        raise

print("###### Nouns ######")
print_first_n(nouns, 3)
with open("nouns.json", "w") as f:
    json.dump(nouns, f)

print("###### adverbs ######")
print_first_n(adverbs, 3)
with open("adverbs.json", "w") as f:
    json.dump(adverbs, f)

print("###### verbs ######")
print_first_n(verbs, 3)
with open("verbs.json", "w") as f:
    json.dump(verbs, f)

print("###### adjektivs ######")
print_first_n(adjektivs, 3)
with open("adjektivs.json", "w") as f:
    json.dump(adjektivs, f)
